import { utils, writeFileXLSX } from 'xlsx'

export function exportXlsxFile(table, name) {
  const ws = utils.json_to_sheet(table)
  // 生成一个workBook
  const wb = utils.book_new()
  // 将sheet数据加到workBook中
  utils.book_append_sheet(wb, ws, 'Data')
  // 写入数据到xlsx并且导出文件
  writeFileXLSX(wb, `${name}.xlsx`)
}

/** 导出带有图片的数据xlsx文件
 * @param list 数据的table文件
 * @param h 图片的长
 * @param w 图片的宽
 */
export function exportXlsxAndImageFile(list, height, width) {
  // 创建工作簿对象
  const workbook = utils.book_new()
  // 创建工作表对象
  const worksheet = utils.aoa_to_sheet([])
  // 图片参数定义
  // const image = {
  //   hyperlink: imgDataUri,
  //   ref: `A${worksheet['!rows'].length + 1}`,
  //   width,
  //   height,
  // }
  list.forEach((row) => {
    const rowData = [
      row.text,
      {
        hyperlink: imgDataUri,
        ref: `A${worksheet['!rows'].length + 1}`,
        width: imgWidth,
        height: imgHeight,
      },
    ]
    XLSX.utils.sheet_add_aoa(worksheet, [rowData])
  })
}

export function consvasToBase64(convas) {
  return convas.toDataURL()
}
