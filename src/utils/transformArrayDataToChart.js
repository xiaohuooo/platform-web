const compareData = {
  flatEp: '平谷时段用电',
  peakEp: '高峰时期用电',
  pointEp: '尖峰时期用电',
  valleyEp: '低谷时段用电',
}
export function transformArrayToChart(array) {
  array.reverse()
  const arr = []
  array.map((item) => {
    ;['flatEp', 'peakEp', 'pointEp', 'valleyEp'].forEach((key) => {
      arr.push({
        year: item.date,
        value: item[key],
        type: compareData[key],
      })
    })
  })
  return arr
}
