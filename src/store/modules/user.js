import { login, logout, getInfo } from '@/api/login'
import { getToken, setToken, removeToken } from '@/utils/auth'
import defAva from '@/assets/images/profile.jpg'

const useUserStore = defineStore('user', {
  state: () => ({
    token: getToken(),
    name: '',
    avatar: '',
    roles: [],
    userRoles: [],
    currentRole: {},
    extraRoles: [],
    permissions: [],
  }),
  getters: {
    projectIdGetter: (state) => {
      if (state.currentRole && state.currentRole.projectId) {
        return state.currentRole.projectId
      }
      if (state.currentRole[0] && state.currentRole[0].projectId) {
        return state.currentRole[0].projectId
      }
      return 1
    },
  },
  actions: {
    // 登录
    login(userInfo) {
      const username = userInfo.username.trim()
      const password = userInfo.password
      const code = userInfo.code
      const uuid = userInfo.uuid
      return new Promise((resolve, reject) => {
        login(username, password, code, uuid)
          .then((res) => {
            let data = res.data
            setToken(data.access_token)
            this.token = data.access_token
            // this.getInfo().then(res=>{
            //   if(!res.userRoles.length){
            //     reject('用户名或密码错误')
            //   }
            // })
            resolve()
          })
          .catch((error) => {
            reject(error)
          })
      })
    },
    // 获取用户信息
    getInfo() {
      return new Promise((resolve, reject) => {
        getInfo()
          .then((res) => {
            const user = res.user
            const avatar =
              user.avatar == '' || user.avatar == null ? defAva : user.avatar

            if (res.roles && res.roles.length > 0) {
              // 验证返回的roles是否是一个非空数组
              this.roles = res.roles
              this.permissions = res.permissions
            } else {
              this.roles = ['ROLE_DEFAULT']
            }
            if (res.userRoles.length) {
              this.permissions = res.permissions
            }
            this.roles = ['ROLE_DEFAULT']
            this.name = user.userName
            this.avatar = avatar
            this.userRoles = res.userRoles
            this.currentRole = res.current || res.userRoles[0]
            const index = this.userRoles.findIndex((item) => {
              if (this.currentRole.projectId) {
                return item.projectId === this.currentRole.projectId
              } else if (this.currentRole.enterpriseId) {
                return (
                  item.enterpriseId === this.currentRole.enterpriseId &&
                  !item.projectId
                )
              } else {
                return false
              }
            })
            const _userRoles = [...res.userRoles]
            _userRoles.splice(index, 1)
            this.extraRoles = _userRoles
            resolve(res)
          })
          .catch((error) => {
            reject(error)
          })
      })
    },
    // 退出系统
    logOut() {
      return new Promise((resolve, reject) => {
        logout(this.token)
          .then(() => {
            this.token = ''
            this.roles = []
            this.permissions = []
            removeToken()
            resolve()
          })
          .catch((error) => {
            reject(error)
          })
      })
    },
    switch(index) {
      const current = this.extraRoles[index]
      const _index = this.userRoles.findIndex((item) => {
        if (item.enterpriseId) {
          return item.enterpriseId === current.enterpriseId
        } else if (item.projectId) {
          return item.projectId === current.projectId
        } else {
          return false
        }
      })
      this.currentRole = this.userRoles[_index]
      this.extraRoles = [...this.userRoles]
      this.extraRoles.splice(_index, 1)
    },
  },
})

export default useUserStore
