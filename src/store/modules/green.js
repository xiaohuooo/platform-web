const useGreenStore = defineStore(
  'green',
  {
    state: () => ({
      electricity:{
        deviceList:[],
        title:''
      }
    }),
    actions:{
      setElectricityDeviceList(payload){
        this.electricity.deviceList=payload
      }
    }
  }
)
export default useGreenStore