import { defineStore } from 'pinia'
import { computed, ref } from 'vue'

export const useDashboardTheme = defineStore('dashboard-theme', () => {
  const theme = ref('你好')
  /** 是否是科技风 tech-科技风 normal-正常 */
  const isTech = ref('normal')

  const settings = reactive({
    tech: {
      /** 主要字体颜色 */
      primaryText: '#fff',
      /** 主背景颜色 */
      bgColor: '#0E213A',
      cardClass: 'card-tech',
      tabActiveBg: 'rgba(51, 142, 255, 0.30)',
      tabBg: 'rgba(12, 37, 55, 0.80)',
      chartXYTextColor: 'rgba(255,255,255,0.6)',
      chartLineGrid: 'rgba(216,216,216,0.3)',
      selectBg: '#B34DB3',
    },
    normal: {
      primaryText: '#333',
      bgColor: '#EFEFF1',
      cardClass: 'card-normal',
      tabActiveBg: '#00A0E9',
      tabBg: '#F4F4F4',
      chartXYTextColor: '#666666',
      chartLineGrid: 'rgba(0, 0, 0, 0.15)',
      selectBg: '#e5e5e5',
    },
  })

  const themeActive = computed(() => {
    return settings[isTech.value]
  })

  const checkIfTech = computed(() => {
    return isTech.value === 'tech'
  })

  function changeTheme() {
    if (isTech.value === 'tech') {
      isTech.value = 'normal'
    } else {
      isTech.value = 'tech'
    }
  }

  return {
    theme,
    isTech,
    settings,
    themeActive,
    changeTheme,
    checkIfTech,
  }
})
