import request from '@/utils/request'
import useUserStore from '@/store/modules/user'

const userStore = useUserStore()

/** 统一的api,接口过多,使用时传入一个对应的url就行 */
export function fetchDashboardData(url, needProId) {
  return request({
    url: `/system/all/${url}/${needProId ? '' : userStore.projectIdGetter}`,
  })
}
