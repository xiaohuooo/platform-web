import request from '@/utils/request'

// 获取路由
export const getRoutersByRoleId = (roleId) => {
  return request({
    url: `/system/menu/getRoutersByRoleId/${roleId}`,
    method: 'get',
  })
}
export const getRouters = () => {
  return request({
    url: '/system/menu/getRouters',
    method: 'get'
  })
}