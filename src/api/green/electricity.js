import request from '@/utils/request';

export function getDeviceStatus(projectId){
  return request({
    url:`/device/electricity/total/${projectId}`,
    method:'get',
  })
}
export function getElectricityUsed(data){
  return request({
    url:"/device/electricity/electricityAnalysis",
    method:'post',
    data
  })
}
export function getZoneList(projectId){
  return request({
    url:`/device/electricityZone/list/${projectId}`,
    method:'get',
  })
}
export function addZone(data){
  return request({
    url:'/device/electricityZone/add',
    method:'post',
    data
  })
}
export function editZone(data){
  return request({
    url:'/device/electricityZone/edit',
    method:'post',
    data
  })
}
export function delDevice(deviceId){
  return request({
    url:`/device/electricityZone/deleteDevice/${deviceId}`,
    method:'post',
  })
}
export function addDevice(data){
  return request({
    url:'/device/electricityZone/addDevice',
    method:'post',
    data
  })
}
export function getPerformance(data){
  return request({
    url:'/device/electricity/electricityFunctionAnalysis',
    method:'post',
    data
  })
}
export function getDeviceList(projectId){
  return request({
    url:`/device/electricity/deviceList/${projectId}`,
    method:'get',
  })
}
export function getWarningInfo(projectId){
  return request({
    url:`/device/electricity/alarmTotal/${projectId}`,
    method:'get',
  })
}
export function getWarningList(data){
  return request({
    url:`/device/sysMessage/listPage`,
    method:'post',
    data,
    notice:false
  })
}
export function getWarningConfig(projectId){
  return request({
    url:`/device/electricityAlarmConfig/get/${projectId}`,
    method:'get',
  })
}
export function setWarningConfig(data){
  return request({
    url:`/device/electricityAlarmConfig/saveOrUpdate`,
    method:'post',
    data
  })
}