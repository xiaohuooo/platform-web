import request from '@/utils/request';

export function getDeviceStatus(projectId){
  return request({
    url:`/device/waterMater/total/${projectId}`,
    method:'get',
  })
}
export function getTotalUse(data){
  return request({
    url:"/device/waterMater/amount/stat",
    method:'post',
    data
  })
}
export function getZoneList(params){
  return request({
    url:'/device/waterMaterZone/list',
    method:'get',
    params
  })
}
export function addZone(data){
  return request({
    url:'/device/waterMaterZone/add',
    method:'post',
    data
  })
}
export function editZone(data){
  return request({
    url:'/device/waterMaterZone/modify',
    method:'post',
    data
  })
}
export function delDevice(deviceCodes){
  return request({
    url:`/device/waterMaterZone/removeWaterMeters/${deviceCodes}`,
    method:'delete',
  })
}
export function addDevice(data){
  return request({
    url:'/device/waterMaterZone/add/waterMeter',
    method:'post',
    params:data
  })
}
export function getFilterUsed(data){
  return request({
    url:'/device/waterMater/waterUsed/stat',
    method:'post',
    data
  })
}
export function getHoursUsed(data){
  return request({
    url:'/device/waterMater/yesterday/stat',
    method:'post',
    data
  })
}
export function getTableData(data){
  return request({
    url:'/device/waterMater/allZone/stat',
    method:'post',
    data
  })
}