import request from '@/utils/request';

export function getDeviceList(projectId){
  return request({
    url:`/device/automaticSprayLog/deviceList/${projectId}`,
    method:'get',
  })
}
export function getRecordList(data){
  return request({
    url:`/device/automaticSprayLog/logList`,
    method:'post',
    data
  })
}
export function batchOpen(data){
  return request({
    url:`/device/automaticSprayLog/manualOpen`,
    method:'post',
    data
  })
}
export function editConfig(data){
  return request({
    url:`/device/automaticSprayConfig/saveOrUpdateConfig`,
    method:'post',
    data
  })
}
export function getConfig(data){
  return request({
    url:`/device/automaticSprayConfig/config`,
    method:'post',
    data
  })
}
export function setTask(data){
  return request({
    url:`/device/automaticSprayConfig/saveOrUpdateRegularConfig`,
    method:'post',
    data
  })
}
export function delTask(regularConfigId){
  return request({
    url:`/device/automaticSprayConfig/deleteById/${regularConfigId}`,
    method:'post',
  })
}