import request from '@/utils/request';

export function getDeviceStatus(projectId){
  return request({
    url:`/device/switch/total/${projectId}`,
    method:'get',
  })
}
export function getDuration(params){
  return request({
    url:`/device/switch/duration`,
    method:'get',
    params
  })
}
export function getDeviceList(projectId){
  return request({
    url:`/device/switchgear/list/${projectId}`,
    method:'get',
  })
}
export function addDevice(data){
  return request({
    url:`/device/switchgear/add`,
    method:'post',
    data
  })
}
export function editDevice(data){
  return request({
    url:`/device/switchgear/edit`,
    method:'post',
    data
  })
}
export function delDevice(id){
  return request({
    url:`/device/switchgear/delete/${id}`,
    method:'post',
  })
}
export function addTask(data){
  return request({
    url:`/device/switch/saveOrUpdateRegular`,
    method:'post',
    data
  })
}
export function delTask(regularId){
  return request({
    url:`/device/switch/deleteById/${regularId}`,
    method:'post',
  })
}
export function switchDevice(regularId){
  return request({
    url:`/device/switch/onOrOff`,
    method:'post',
  })
}
export function switchTask(params){
  return request({
    url:`/device/switch/enableOrDisable`,
    method:'post',
    params
  })
}
export function getChartData(data){
  return request({
    url:`/device/switch/trend`,
    method:'post',
    data
  })
}
export function getTaskList(projectId){
  return request({
    url:`/device/switch/regular/list/${projectId}`,
    method:'post',
  })
}
export function getRecord(data){
  return request({
    url:`/device/switch/record`,
    method:'post',
    data
  })
}