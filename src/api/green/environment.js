import request from '@/utils/request';

export function getDeviceStatus(projectId){
  return request({
    url:`/device/environment/total/${projectId}`,
    method:'get',
  })
}
export function getDeviceList(projectId){
  return request({
    url:`/device/environment/deviceList/${projectId}`,
    method:'get',
  })
}
export function getWarningInfo(data){
  return request({
    url:`/device/environment/alarmTotal`,
    method:'post',
    data
  })
}
export function getWarningRecord(data){
  return request({
    url:`/device/sysMessage/listPage`,
    method:'post',
    data
  })
}
export function getTrend(data){
  return request({
    url:`/device/environment/trend`,
    method:'post',
    data
  })
}
export function getTableData(data){
  return request({
    url:`/device/environment/pageList`,
    method:'post',
    data
  })
}
export function getConfig(deviceId){
  return request({
    url:`/device/environment/configByDeviceNo/${deviceId}`,
    method:'get',
  })
}
export function setConfig(data){
  return request({
    url:`/device/environment/editConfig`,
    method:'post',
    data
  })
}
export function download(data){
  return request({
    url:`/device/environment/export`,
    method:'post',
    data
  })
}
export function getLatest(deviceId){
  return request({
    url:`/device/environment/getLastDate/${deviceId}`,
    method:'get',
  })
}