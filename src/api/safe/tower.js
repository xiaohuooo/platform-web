import request from '@/utils/request';

export function getDeviceList(projectId){
  return request({
    url:`/device/tower/deviceList/${projectId}`,
    method:'get',
  })
}
export function getDeviceInfo(deviceId){
  return request({
    url:`/device/tower/attrData/${deviceId}`,
    method:'get',
  })
}
export function getTrend(data){
  return request({
    url:`/device/tower/trend`,
    method:'post',
    data
  })
}
export function getTransportTotal(data){
  return request({
    url:`/device/tower/transportTotal`,
    method:'post',
    data
  })
}
export function getRecordList(data){
  return request({
    url:`/device/tower/workRecordList`,
    method:'post',
    data
  })
}
export function getWarnInfo(projectId){
  return request({
    url:`/device/tower/towerAlarmTotal/${projectId}`,
    method:'get',
  })
}
export function getWarnList(data){
  return request({
    url:`/device/tower/alarmList`,
    method:'post',
    data
  })
}
export function getAntiCollision(projectId){
  return request({
    url:`/device/tower/antiCollision/${projectId}`,
    method:'get',
  })
}