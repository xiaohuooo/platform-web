import request from '@/utils/request';

export function getDeviceList(data){
  return request({
    url:`/device/unload/list`,
    method:'get',
    params:data
  })
}
export function getDeviceInfo(data){
  return request({
    url:`/device/unload/total`,
    method:'get',
    params:data
  })
}
export function getWarnInfo(data){
  return request({
    url:`/device/unload/alarm/stat`,
    method:'post',
    data
  })
}
export function getWarnRecord(data){
  return request({
    url:`/device/unload/alarm/record`,
    method:'post',
    data
  })
}