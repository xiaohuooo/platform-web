import request from '@/utils/request'

// 查询监控列表
export function getVideoList(query) {
  return request({
    url: '/system/videoCameras/list',
    method: 'get',
    params: query,
  })
}
//查询监控点
export function getRegionList(query) {
  return request({
    url: '/system/videoRegion/list',
    method: 'get',
    params: query,
  })
}
//查询监控点链接
export function getPreviewUrl(query) {
  return request({
    url: '/system/videoCameras/previewUrl',
    method: 'get',
    params: query,
  })
}
//获取hls流
export function getHlsPath(query) {
  return request({
    url: '/system/videoCameras/getHlsPath',
    method: 'get',
    params: query,
  })
}

export function getRtspPath(query) {
  return request({
    url: '/system/videoCameras/getRtspPath',
    method: 'get',
    params: query,
  })
}

//rtmp任务心跳处理
export function rtmpTaskHeartbeat(query) {
  return request({
    url: '/system/videoCameras/rtmpTaskHeartbeat',
    method: 'get',
    params: query,
  })
}
//关闭rtmp流
export function closeRtmpTask(query) {
  return request({
    url: '/system/videoCameras/closeRtmpTask',
    method: 'get',
    params: query,
  })
}
