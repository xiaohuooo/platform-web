import request from '@/utils/request'
import md5 from 'blueimp-md5'
/**
 * 添加文件夹
 * @param data
 */
export function addFolder(data) {
  return request({
    baseURL: '/api',
    url: '/bim/model2/model/folder/add',
    method: 'post',
    data,
  })
}

/**
 *上传文件uploadFileBySign 添加签名
 ------------------------------------------------------------第二个接口要调md5
 * @param  {params} 文件参数
 * @param {urlParam} 上传文件地址
 * @returns
 */
 export function uploadFileBySign(params, urlParam = null) {
  var promise = new Promise((resolve, reject) => {
    const FILE = params.file
    // 签名
    const SIGN = getSignForFile({ fileName: FILE.name, fileSize: FILE.size })
    var uploadAddress = urlParam ? urlParam + 'api/bim/dms/documents/upload/' : process.env.MODEL_UPLOAD + 'api/bim/dms/documents/upload/' + SIGN
    const FORM = new FormData()
    FORM.append('file', FILE)
    const XHR = new XMLHttpRequest()
    XHR.upload.onloadstart = () => {
      console.warn(params.file.name + '开始上传,上传开始时间:' + new Date())
    }
    XHR.open('post', uploadAddress, true)

    XHR.setRequestHeader('token', getToken())
    XHR.send(FORM)
    XHR.ontimeout = (event) => {
      reject('timeout')
    }
    XHR.onerror = (event) => {
      reject('request error')
    }
    XHR.onload = () => {
      try {
        console.warn(params.file.name + '上传结束,上传结束时间:' + new Date())
        const RESPONSE = JSON.parse(XHR.response)
        if (RESPONSE.code === 0) {
          var backResponse = {
            name: RESPONSE.data.name,
            size: RESPONSE.data.size,
            url: RESPONSE.data.storagePathOrigin,
            result: true,
            state: 'SUCCESS',
            objectId: RESPONSE.data.objectId,
            extensionName: RESPONSE.data.extensionName,
            storagePath: RESPONSE.data.storagePath
          }
          resolve(backResponse)
        } else {
          Message({ type: 'error', message: RESPONSE.errmsg, showClose: true })
          reject(new Error(RESPONSE.message))
        }
      } catch (e) {
        reject('error')
      }
    }
  })
  return promise
}
// // 文件上传添加header签名sign
export function getSignForFile(params) {
  const FINAL = []
  getSign(params, '', FINAL)
  return getFinallstr(FINAL)
}
// 文件读取后加上sign
// params:传入 objectId
export function openFileBySign(params) {
  return process.env.FILE_ACCESS + params + '&sign=' + getSignForFile(params)
}
export function getFinallstr(params) {
  const newArr = Array.from(new Set(params))
  const urlStr = newArr.sort().join('&')
  if (urlStr) {
    return md5(`${urlStr}&${process.key.md5Key}`)
  } else {
    return md5(process.key.md5Key)
  }
}

export function getSign(params, key = '', resultArr = []) {
  if (params === undefined) {
    return ''
  }
  if (typeof params === 'string' ||
    typeof params === 'number' ||
    typeof params === 'boolean' ||
    params === null) {
    // 若参数为字符串、数字\boolen类型
    key ? resultArr.push(key + params) : resultArr.push(params)
  } else if (typeof params === 'object') {
    if (Array.isArray(params) && params.length > 0) {
      // 数组格式
      params.forEach(val => {
        getSign(val, '', resultArr)
      })
    } else if (!isNaN(Date.parse(params))) {
      // 时间格式
      key ? resultArr.push(key + params) : resultArr.push(params)
    } else {
      for (var i in params) {
        if (Array.isArray(params[i]) && params[i].length > 0) {
          // 若为数组,判断子节点: 若为对象,则接着递归,若为字符串或数字,则提取出来key和值
          params[i].forEach(val => {
            getSign(val, i, resultArr)
          })
        } else if (typeof params[i] !== 'undefined') {
          // 不为数组,则把对象提取出来
          getSign(params[i], i, resultArr)
        }
      }
    }
  }
}


/* 获取模型文件documentID -------------第三个接口*/
export function addOrUpgradeFile(data) {
  return request({
    timeout: 0,
    baseURL: process.env.MODEL_UPLOAD,
    url: 'api/bim/dms/documents/1/addOrUpgradeFile',
    method: 'post',
    data
  })
}


/* 模型文件转换接口 --------------第四个接口*/
export function modelConversion(data) {
  return request({
    baseURL: process.env.MODEL_CONVERSION,
    url: '/v1/bim/review/executor/init',
    method: 'post',
    data
  })
}


/**
 * 上传模型文件------第五个接口
 * @param data
 */
export function uploadFile(data) {
  return request({
    timeout: 0,
    baseURL: process.env.BUSINESS_API,
    url: '/bim/model2/manage/modelUpload',
    method: 'post',
    data: data
  })
}

// 删除
export function getDelete(id) {
  return request({
    url: `/system/intelligenceBuild/delete/${id}`,
    method: 'delete',
  })
}
//下载文件
export function getDownload(query) {
  return request({
    url: '/system/intelligenceBuild/documents/download',
    method: 'get',
    params: query,
  })
}
/**
 * 获取所有模型列表
 * @param data
 */
export function treeWithFile(data) {
  return request({
    baseURL: process.env.BUSINESS_API,
    url: '/bim/model2/model/folder/treeWithFile',
    method: 'post',
    data,
  })
}
//上传文件
export function getPreviewUrl(data) {
  return request({
    url: '/system/intelligenceBuild/documents/upload',
    method: 'post',
    data: data,
  })
}
//获取
export function getDetail(id) {
  return request({
    url: `/system/intelligenceBuild/findById/${id}`,
    method: 'get',
    params: query,
  })
}

export function getList(query) {
  return request({
    url: '/system/intelligenceBuild/list',
    method: 'get',
    params: query,
  })
}
