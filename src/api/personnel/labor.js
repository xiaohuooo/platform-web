import request from '@/utils/request'
import useUserStore from '@/store/modules/user'
import dayjs from 'dayjs'

const userStore = useUserStore()

/** 获取列表 */
export function fetchGetLaborList() {
  return request.get('/system/labor/list')
}

/** 获取列表，含进出场时间 */
export function fetchGetLaborListTime() {
  return request.get('/system/labor/listWithAttendance')
}

/** 获取分页列表(含进出场时间) */
export function fetchGetLaborListPageTime() {
  return request.get('/system/labor/pageListWithAttendance')
}

/** 获取分页列表(不含进出场时间) */
export function fetchGetLaborListPage() {
  return request.get('/system/labor/pageList')
}

/** 审核 */
export function fetchGetAuditLabor(id, status) {
  return request.get(`/system/labor/audit/${id}`, { params: { status } })
}

/** 删除 */
export function fetchDeleteLabor(id) {
  return request.delete(`/system/labor/delete/${id}`)
}

/** 获取逐时段项目人数 */
export function fetchGetDaypartingAnalysis() {
  const date = dayjs(Date.now()).format('YYYY-MM-DD HH:mm:SS')
  return request.get('/system/labor/daypartingAnalysis', {
    params: { projectId: userStore.projectIdGetter, currentDate: date },
  })
}

/** 工种分析获取 */
export function fetchGetWorkTypeAnalysis() {
  return request.get('/system/labor/workTypeAnalysis', {
    params: { projectId: userStore.projectIdGetter },
  })
}

/** 工人年龄段分析 */
export function fetchGetAgeAnalysis() {
  return request.get('/system/labor/ageAnalysis', {
    params: { projectId: userStore.projectIdGetter },
  })
}

/** 分包单位分析 */
export function fetchGetSubContractorAnalysis() {
  return request.get('/system/labor/subContractorAnalysis', {
    params: { projectId: userStore.projectIdGetter },
  })
}
