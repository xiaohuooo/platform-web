import request from '@/utils/request'

/** 获取列表信息 */
export function fetchGetPartyConstructList(query, name) {
  return request({
    url: `/system/partyConstruct/${name}/list`,
    method: 'get',
    params: query,
  })
}

export function fetchChangepartyConstruct(data) {
  return request({
    url: '/system/partyConstruct/modify',
    method: 'post',
    data,
  })
}

export function fetchAddpartyConstruct(params, name) {
  return request({
    url: `/system/partyConstruct/${name}/add`,
    method: 'post',
    data: params,
  })
}

export function fetchDeletepartyConstruct(id) {
  return request({
    url: `/system/partyConstruct/delete/${id}`,
    method: 'delete',
  })
}

export function fetchChangePartyConstructSort(data) {
  return request({
    url: '/system/partyConstruct/sort',
    method: 'post',
    data,
  })
}
