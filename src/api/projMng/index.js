import request from '@/utils/request';

export function getProjList(data){
  return request({
    url:'/system/sysProject/listPage',
    method:'post',
    data
  })
}
export function addProject(data){
  return request({
    url:'/system/sysProject/add',
    method:'post',
    data
  })
}
export function editProject(data){
  return request({
    url:'/system/sysProject/edit',
    method:'post',
    data
  })
}
export function deleteProject(id){
  return request({
    url:`/system/sysProject/delete/${id}`,
    method:'post',
  })
}