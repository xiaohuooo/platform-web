import request from '@/utils/request';

export function switchOrganize(roleId){
  return request.get(`/system/user/${roleId}`)
}

export function getOrganizeTree(data){
  return request({
    url:'/system/dept/childrenTreeByDeptId',
    method:'post',
    data
  })
}
export function addOrganize(data){
  return request({
    url:'/system/dept',
    method:'post',
    data
  })
}
export function deleteOrganize(deptId,type){
  return request({
    url:`/system/dept/${deptId}/${type}`,
    method:'delete',
  })
}
export function editOrganize(data){
  return request({
    url:'/system/dept',
    method:'put',
    data
  })
}
export function getUserList(data){
  return request({
    url:'/system/user/listByDeptId',
    method:'get',
    params:data
  })
}
export function switchRole(data){
  return request({
    url:'/system/lastLogin/update',
    method:'post',
    data:data
  })
}
