import request from '@/utils/request';

export function getUserList(data){
  return request({
    url:'/system/user/list',
    method:'get',
    params:data
  })
}

export function addUser(data){
  return request({
    url:'/system/user',
    method:'post',
    data
  })
}
export function editUser(data){
  return request({
    url:'/system/user',
    method:'put',
    data
  })
}
export function delUser(data){
  return request({
    url:`/system/user/remove`,
    method:'delete',
    data
  })
}
export function getPermissionById(roleId){
  return request({
    url:`/system/role/${roleId}`,
    method:'get',
  })
}
export function getRoleInfoTree(enterpriseId){
  return request({
    url:`/system/role/projectByEnterpriseId/${enterpriseId}`,
    method:'get',
  })
}
export function getEditRoleInfoTree(data){
  return request({
    url:`/system/role/projectByEnterpriseIdEcho`,
    method:'post',
    data
  })
}