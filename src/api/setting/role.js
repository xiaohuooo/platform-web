import request from '@/utils/request';

export function getRoleList(data){
  return request({
    url:'/system/role/list',
    method:'get',
    params:data
  })
}

export function addRole(data){
  return request({
    url:'/system/role',
    method:'post',
    data
  })
}
export function editRole(data){
  return request({
    url:'/system/role',
    method:'put',
    data
  })
}
export function delRole(roleIds){
  return request({
    url:`/system/role/${roleIds}`,
    method:'delete',
    notice:false
  })
}
export function getPermissionById(roleId){
  return request({
    url:`/system/role/${roleId}`,
    method:'get',
  })
}
export function getPermissionList(data){
  return request({
    url:'/system/menu/listMenu',
    method:'get',
    params:data
  })
}