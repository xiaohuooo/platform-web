import request from '@/utils/request'

export function fetchGetTourData(query) {
  return request({
    url: '/system/device/tour/pageList',
    method: 'post',
    data: query,
  })
}

export function fetchGetTourOneData(id) {
  return request({
    url: `/system/device/tour/detail/${id}`,
    method: 'get',
  })
}
