import request from '@/utils/request'

export function fetchGetDeviceBaseData(query) {
  return request({
    url: '/system/deviceParams/list',
    method: 'get',
    params: query,
  })
}

export function fetchGetDeviceBaseOneData(id) {
  return request({
    url: `/system/deviceParams/get/${id}`,
    method: 'get',
  })
}

export function fetchChangeDeviceBase(data) {
  return request({
    url: '/system/deviceParams/modify',
    method: 'post',
    data,
  })
}

export function fetchAddDeviceBase(params) {
  return request({
    url: '/system/deviceParams/add',
    method: 'post',
    data: params,
  })
}

export function fetchDeleteDeviceBase(id) {
  return request({
    url: `/system/deviceParams/delete/${id}`,
    method: 'delete',
  })
}

export function fetchDownLoadTemplate() {
  return request({
    url: '/system/deviceParams/importTemplate',
    method: 'post',
    header: { 'Content-Type': 'application/xls' },
    responseType: 'blob',
  })
}

export function fetchGetParamTypeList() {
  return request({
    url: '/system/deviceParams/getParamTypeList',
    method: 'get',
  })
}
