import request from '@/utils/request'

export function fetchGetDeviceCatalogData(query) {
  return request({
    url: '/system/deviceCatalog/list',
    method: 'get',
    params: query,
  })
}

export function fetchGetDeviceCatalogOneData(id) {
  return request({
    url: `/system/deviceCatalog/get/${id}`,
    method: 'get',
  })
}

export function fetchChangeDeviceCatalog(data) {
  return request({
    url: '/system/deviceCatalog/modify',
    method: 'post',
    data,
  })
}

export function fetchAddDeviceCatalog(params) {
  return request({
    url: '/system/deviceCatalog/add',
    method: 'post',
    data: params,
  })
}

export function fetchDeleteDeviceCatalog(id) {
  return request({
    url: `/system/deviceCatalog/delete/${id}`,
    method: 'delete',
  })
}
