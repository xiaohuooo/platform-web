// 设备信息相关接口
import request from '@/utils/request'

export function fetchGetDeviceInfoData(query) {
  return request({
    url: '/system/deviceInfo/pageList',
    method: 'get',
    params: query,
  })
}

export function fetchGetDeviceInfoOneData(id) {
  return request({
    url: `/system/deviceInfo/get/${id}`,
    method: 'get',
  })
}

export function fetchChangeDeviceInfo(data) {
  return request({
    url: '/system/deviceInfo/modify',
    method: 'post',
    data,
  })
}

export function fetchAddDeviceInfo(params) {
  return request({
    url: '/system/deviceInfo/add',
    method: 'post',
    data: params,
  })
}

export function fetchDeleteDeviceInfo(id) {
  return request({
    url: `/system/deviceInfo/delete/${id}`,
    method: 'delete',
  })
}

export function fetchDownLoadTemplate(params) {
  return request({
    url: '/system/deviceInfo/importTemplate',
    method: 'post',
    header: { 'Content-Type': 'application/xls' },
    responseType: 'blob',
    params,
  })
}

export function fetchImportData(params) {
  return request({
    url: '/system/deviceInfo/importData',
    method: 'post',
    params,
  })
}

export function fetchExportDevicecData(params) {
  return request({
    url: '/system/deviceInfo/export',
    method: 'post',
    header: { 'Content-Type': 'application/xls' },
    responseType: 'blob',
    params,
  })
}

export function fetchExportQrcode(params) {
  return request({
    url: '/system/deviceInfo/exportQrCode',
    method: 'post',
    header: { 'Content-Type': 'arraybuffer' },
    responseType: 'blob',
    params,
  })
}

export function fetchGetQrcodeImages(id) {
  return request({
    url: `/system/deviceInfo/qrCode/${id}`,
    responseType: 'blob',
    method: 'get',
  })
}

export function fetchSyncDeviceIotInfo(proId) {
  return request({
    url: '/system/deviceInfo/syncDevice',
    method: 'get',
    params: { projectId: proId },
  })
}
