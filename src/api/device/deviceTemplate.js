import request from '@/utils/request'

export function fetchGetDeviceTemplateData(query) {
  return request({
    url: '/system/deviceTemplate/list',
    method: 'get',
    params: query,
  })
}

export function fetchGetdeviceTemplateOneData(id) {
  return request({
    url: `/system/deviceTemplate/get/${id}`,
    method: 'get',
  })
}

export function fetchChangedeviceTemplate(data) {
  return request({
    url: '/system/deviceTemplate/batchModify',
    method: 'post',
    data,
  })
}

export function fetchAdddeviceTemplate(params) {
  return request({
    url: '/system/deviceTemplate/batchAdd',
    method: 'post',
    data: params,
  })
}

export function fetchDeletedeviceTemplate(id) {
  return request({
    url: `/system/deviceTemplate/delete/${id}`,
    method: 'delete',
  })
}
