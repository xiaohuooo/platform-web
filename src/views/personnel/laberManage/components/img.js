import task from '../../../../assets/images/personnel/task.jpg'
import clock from '../../../../assets/images/personnel/clock.jpg'
import lineImg from '../../../../assets/images/personnel/line.jpg'
import img1 from '../../../../assets/images/personnel/img1.jpg'
import four from '../../../../assets/images/personnel/four.png'
import second from '../../../../assets/images/personnel/second.jpg'
import third from '../../../../assets/images/personnel/third.jpg'
import first from '../../../../assets/images/personnel/first.jpg'

export { task, clock, lineImg, img1, four, second, third, first }
