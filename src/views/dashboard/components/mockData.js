/** 堆叠数据模拟数据 */
export const cloumnData = [
  {
    year: '1991',
    value: 3,
    type: '高峰时段用电',
  },
  {
    year: '1992',
    value: 4,
    type: '高峰时段用电',
  },
  {
    year: '1993',
    value: 3.5,
    type: '高峰时段用电',
  },
  {
    year: '1994',
    value: 5,
    type: '高峰时段用电',
  },
  {
    year: '1995',
    value: 4.9,
    type: '高峰时段用电',
  },
  {
    year: '1996',
    value: 6,
    type: '高峰时段用电',
  },
  {
    year: '1997',
    value: 7,
    type: '高峰时段用电',
  },
  {
    year: '1998',
    value: 9,
    type: '高峰时段用电',
  },
  {
    year: '1999',
    value: 13,
    type: '高峰时段用电',
  },
  {
    year: '1991',
    value: 3,
    type: '尖峰时期用电',
  },
  {
    year: '1992',
    value: 4,
    type: '尖峰时期用电',
  },
  {
    year: '1993',
    value: 3.5,
    type: '尖峰时期用电',
  },
  {
    year: '1994',
    value: 5,
    type: '尖峰时期用电',
  },
  {
    year: '1995',
    value: 4.9,
    type: '尖峰时期用电',
  },
  {
    year: '1996',
    value: 6,
    type: '尖峰时期用电',
  },
  {
    year: '1997',
    value: 7,
    type: '尖峰时期用电',
  },
  {
    year: '1998',
    value: 9,
    type: '尖峰时期用电',
  },
  {
    year: '1999',
    value: 13,
    type: '尖峰时期用电',
  },
  {
    year: '1991',
    value: 3,
    type: '低谷时期用电',
  },
  {
    year: '1992',
    value: 4,
    type: '低谷时期用电',
  },
  {
    year: '1993',
    value: 3.5,
    type: '低谷时期用电',
  },
  {
    year: '1994',
    value: 5,
    type: '低谷时期用电',
  },
  {
    year: '1995',
    value: 4.9,
    type: '低谷时期用电',
  },
  {
    year: '1996',
    value: 6,
    type: '低谷时期用电',
  },
  {
    year: '1997',
    value: 7,
    type: '低谷时期用电',
  },
  {
    year: '1998',
    value: 9,
    type: '低谷时期用电',
  },
  {
    year: '1999',
    value: 13,
    type: '低谷时期用电',
  },
  {
    year: '1991',
    value: 3,
    type: '平谷时段用电',
  },
  {
    year: '1992',
    value: 4,
    type: '平谷时段用电',
  },
  {
    year: '1993',
    value: 3.5,
    type: '平谷时段用电',
  },
  {
    year: '1994',
    value: 5,
    type: '平谷时段用电',
  },
  {
    year: '1995',
    value: 4.9,
    type: '平谷时段用电',
  },
  {
    year: '1996',
    value: 6,
    type: '平谷时段用电',
  },
  {
    year: '1997',
    value: 7,
    type: '平谷时段用电',
  },
  {
    year: '1998',
    value: 9,
    type: '平谷时段用电',
  },
  {
    year: '1999',
    value: 13,
    type: '平谷时段用电',
  },
]

/** 单一柱状图 */
export const columnSingleData = [
  {
    year: '塔机',
    value: 3,
    type: '设备数量',
  },
  {
    year: '电表',
    value: 4,
    type: '设备数量',
  },
  {
    year: '水表',
    value: 3.5,
    type: '设备数量',
  },
  {
    year: '传感器',
    value: 5,
    type: '设备数量',
  },
  {
    year: '升降机',
    value: 4.9,
    type: '设备数量',
  },
  {
    year: '智能开关',
    value: 4.9,
    type: '设备数量',
  },
]

/** 折线图数据源 */
export const lineData = [
  { year: '1991', value: 3 },
  { year: '1992', value: 4 },
  { year: '1993', value: 3.5 },
  { year: '1994', value: 5 },
  { year: '1995', value: 4.9 },
  { year: '1996', value: 6 },
  { year: '1997', value: 7 },
  { year: '1998', value: 9 },
  { year: '1999', value: 13 },
]

/** 模拟折线图顶部tab数据 */
export const lineTopData = [
  {
    label: '温度',
    value: 'temperature',
  },
  {
    label: '湿度',
    value: 'humidity',
  },
  {
    label: '噪音',
    value: 'noise',
  },
  {
    label: 'PM10',
    value: 'pm10',
  },
  {
    label: 'PM2.5',
    value: 'pm25',
  },
  {
    label: '风速',
    value: 'windSpeed',
  },
]

export const vedioData = [
  {
    label: '工地东门摄像头',
    value: 'windRate1',
  },
  {
    label: '工地西门摄像头',
    value: 'windRaste1',
  },
  {
    label: '工地东门摄像头2',
    value: 'windRatedd2',
  },
  {
    label: '施工办公室',
    value: 'windRate2',
  },
  {
    label: '塔机摄像头',
    value: 'windRate3',
  },
  {
    label: '升降机摄像头',
    value: 'windRate4',
  },
  {
    label: '基坑摄像头',
    value: 'windRate5',
  },
]

export const selectData = [
  {
    value: 'Option1',
    label: 'F1层观测点',
  },
  {
    value: 'Option2',
    label: 'F2层观测点',
  },
]

export const selectData2 = [
  {
    value: 'Option1',
    label: '近十日用电量统计',
  },
  {
    value: 'Option2',
    label: '近九日用电量统计',
  },
]

export const pieData = [
  { type: '木工课程', value: 27 },
  { type: '电工课程', value: 25 },
  { type: '水暖工课程', value: 18 },
  { type: '操作类课程', value: 15 },
]
